#ifndef Food_h
#define Food_h

#include "Scene.h"

class Food : public ISceneItem
{
	const char symbol_;
	Position position_;

public:
	Food(Position position) : symbol_('%'), position_(position) {}

	void placeOnScene(Scene& scene) const override { scene.draw(position_, symbol_); }
	void setPosition(Position position) { position_ = position; }
	Position getPosition() { return position_; }
};

#endif