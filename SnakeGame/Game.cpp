#include "Game.h"

#include <windows.h>
#include <thread>
#include <iostream>

//initialiaze for launch with default data
Game::Game() : snake_({ 1, 1 }), food_({ 5, 7 }), speed_(1), view_(make_unique<ConsoleView>())
{
	//add items on play board
	scene_.insertItem(&snake_);
	scene_.insertItem(&food_);
}

void Game::start()
{
	//create threads for user input and snake move
	thread gameThread(&Game::game, this);
	thread controlThread(&Game::control, this);

	//wait threads to finish
	gameThread.join();
	controlThread.join();
}

void Game::repositionFood()
{
	//calculate new position for food
	int x = rand() % scene_.getWidth();
	int y = rand() % scene_.getHeight();

	food_.setPosition({ x, y });
}

bool Game::isBorder(const Position & position)
{
	return position.x_ < 0 || position.y_ < 0 ||
		position.x_ >= scene_.getWidth() ||
		position.y_ >= scene_.getHeight();
}

void Game::control()
{
	while (!gameOver_)
	{
		Snake::Direction direction(snake_.getDirection());

		if (GetAsyncKeyState(VK_UP) && snake_.getDirection() != Snake::Direction::DOWN)
			direction = Snake::Direction::UP;
		else if (GetAsyncKeyState(VK_DOWN) && snake_.getDirection() != Snake::Direction::UP)
			direction = Snake::Direction::DOWN;
		else if (GetAsyncKeyState(VK_LEFT) && snake_.getDirection() != Snake::Direction::RIGHT)
			direction = Snake::Direction::LEFT;
		else if (GetAsyncKeyState(VK_RIGHT) && snake_.getDirection() != Snake::Direction::LEFT)
			direction = Snake::Direction::RIGHT;

		if (direction != snake_.getDirection())
		{
			//only one thread can change the direction
			unique_lock<shared_mutex> lock(controlMutex_);
			snake_.setDirection(direction);
		}
	}
}

void Game::displayScore()
{
	cout << endl << "Score: " << (snake_.getSize()-2) * 10;
}

void Game::game()
{
	while (true)
	{
		{
			//all threads can read the direction but only one can change 
			shared_lock<shared_mutex> lock(controlMutex_);
			snake_.move();
		}

		if (snake_.isBiteItself() || isBorder(snake_.getHeadPosition()))
		{
			gameOver_ = true;
			break;
		}

		if (snake_.getHeadPosition() == food_.getPosition())
		{
			snake_.grow();
			++speed_;
			repositionFood();
		}

		//render dymanic objects on scene
		scene_.render(view_.get());

		displayScore();

		//slow down
		this_thread::sleep_for(std::chrono::milliseconds(100/speed_));

		system("cls");
	}

}
