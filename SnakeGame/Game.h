#ifndef Game_h
#define Game_h

#include "Snake.h"
#include "Food.h"
#include "IView.h"

#include <shared_mutex>

class Game
{
	Snake snake_;
	Food food_;
	Scene scene_;
	unique_ptr<IView> view_;
	shared_mutex controlMutex_;
	bool gameOver_;
	size_t speed_;

public:
	Game();
	void start();

private:
	void repositionFood();
	bool isBorder(const Position& position);
	void control();
	void displayScore();
	void game();	
};

#endif