#ifndef ISceneItem_h
#define ISceneItem_h

class Scene;
struct ISceneItem
{
	virtual void placeOnScene(Scene&) const = 0;
};

#endif