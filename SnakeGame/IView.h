#ifndef IView_h
#define IView_h

#include <vector>
#include <iostream>

using namespace std;

struct IView
{
	 virtual void drawView(vector<vector<char>> field) = 0;
};

class ConsoleView : public IView
{

public:

	void drawView(vector<vector<char>> field) override
	{
		for (const auto& row : field)
		{
			for (const auto& cell : row)
			{
				cout << cell;
			}
			cout << endl;
		}
	}
};

#endif