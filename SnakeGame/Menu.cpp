#include "Menu.h"

#include <iostream>

void Menu::start_game()
{
	Game game_;
	game_.start();

	display();
}

void Menu::exit_game()
{
	exit(0);
}

void Menu::display_input_error()
{
	cout << "Input error" << endl;
	check_input();
}

void Menu::check_input()
{
	char input{};
	cin >> input;

	switch (input)
	{
	case START:
		start_game();
		break;
	case EXIT:
		exit_game();
		break;
	default:
		display_input_error();
		break;
	}
}

void Menu::display()
{
	cout << "Press " << START << " for start the game" << endl << "Press " << EXIT << " for exit" << endl;
	
	check_input();
}

