#ifndef Menu_h
#define Menu_h

#include "Game.h"

class Menu
{
private:

	static const char  START{ 's' };
	static const char EXIT{ 'e' };
	void start_game();
	void exit_game();
	void display_input_error();
	void check_input();
	Game game_;

public:
	
	void display();
};
#endif
