#ifndef Position_h
#define Position_h

struct Position
{
	Position(int x, int y) : x_(x), y_(y) {}

	int x_;
	int y_;

	bool operator==(const Position& position) { return x_ == position.x_ && y_ == position.y_; }
};
#endif