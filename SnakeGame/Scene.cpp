#include "Scene.h"

#include <algorithm>
#include <iostream>

Scene::Scene(size_t height, size_t width) : height_(height), width_(width)
{
	//set size, vector contais height_ of empty vectors
	field_.resize(height_);
	for (auto& cell : field_)
	{
		//set size width_ for all vectors
		cell.resize(width_);
	}
}

void Scene::insertItem(ISceneItem* item)
{
	items_.push_back(item);
}

//render dynamic objects
void Scene::render(IView* view)
{
	//all items know how draw themselves;
	for (auto& item : items_)
	{
		item->placeOnScene(*this);
	}
		
	//print 2d vector to console
	view->drawView(field_);
}

void Scene::draw(Position position, char partical)
{
	field_[position.y_][position.x_] = partical;
}

