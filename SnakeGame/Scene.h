#ifndef Scene_h
#define Scene_h

#include "ISceneItem.h"
#include "IView.h"
#include "Position.h"

#include <vector>

using namespace std;

class Scene
{
	enum DefaltSize { HEIGHT = 20, WIDTH = 50 };
	const size_t height_;
	const size_t width_;
	vector<vector<char>> field_;
	vector<ISceneItem*> items_;

public:
	Scene(size_t height = HEIGHT, size_t width = WIDTH);

	void insertItem(ISceneItem* item);
	void render(IView* view);
	int getWidth() const { return width_; }
	int getHeight() const { return height_; }
	void draw(Position position, char partical);
};
#endif
