#include "Snake.h"

//create Snale with default data
Snake::Snake(Position position) : bodySymbol_('*'), headSymbol_('@'), tailSymbol_(' '), direction_(RIGHT)
{
	//put head and tail symbols
	body_.push_back(position);
	body_.push_back(position);
}

void Snake:: placeOnScene(Scene& scene) const 
{
	//draw head
	scene.draw(body_[0], headSymbol_);
	//draw body
	for (size_t i(1); i < body_.size() - 1; ++i)
		scene.draw(body_[i], bodySymbol_);
	//draw not visible tail symbol; not need to clear the screen
	scene.draw(body_[body_.size() - 1], tailSymbol_);
}

void Snake::grow()
{
	//take last element(tail) and add it to vector
	body_.push_back(body_.back());
}

void Snake::move()
{
	Position next = { 0, 0 };
	//detect position for moving
	switch (direction_)
	{
	case UP:
		--next.y_;
		break;
	case DOWN:
		++next.y_;
		break;
	case LEFT:
		--next.x_;
		break;
	case RIGHT:
		++next.x_;
	}

	//move positions to one cell
	for (size_t i(body_.size() - 1); i > 0; --i)
	{
		body_[i] = body_[i - 1];
	}

	//move head
	body_[0].x_ += next.x_; //TODO create +  operator in position 
	body_[0].y_ += next.y_;
}

bool Snake::isBiteItself()
{
	//if head position the same as tail position
	return body_.end() != find(body_.begin() + 1, body_.end(), body_[0]);
}

size_t Snake::getSize()
{
	return body_.size();
}
