#ifndef Snake_h
#define Snake_h

#include "Scene.h"

using namespace std;

class Snake : public ISceneItem
{
public:
	enum Direction { UP, DOWN, LEFT, RIGHT };
	Snake(Position position);
	void placeOnScene(Scene& scene) const override;
	void grow();
	void move();
	Position getHeadPosition() const { return body_[0]; };
	bool isBiteItself();
	Direction getDirection() { return direction_; }
	void setDirection(Direction direction) { direction_ = direction; }
	size_t getSize();

private:
	char tailSymbol_;
	char bodySymbol_;
	char headSymbol_;
	vector<Position> body_;
	Direction direction_;
};
#endif
